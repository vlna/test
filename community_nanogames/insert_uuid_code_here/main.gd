extends Node


# Tells the NanogamePlayer when the game ends, and if the player either won or
# lost, stopping the timer.
signal ended(has_won)


# Called when the nanogame is instantiated, giving the current difficulty, and
# the debug code (which a specific value can be given in the NanogameDebugger).
func nanogame_prepare(difficulty: int, debug_code: int) -> void:
	pass


# Called when the kickoff ends and the player gains control.
#func nanogame_start() -> void:
#	pass
